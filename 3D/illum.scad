
module plate_stl() {
  color("#605040") import("384_Well_Microplate/384_Well_Microplate.STL");
}

small_array = 0;

xrange = small_array ? [0:4] : [0:23];
yrange = small_array ? [0:10] :[0:15];

xoffset = 12.13;
yoffset = 8.99;

$fn = 25;

module plate_mod() {
  difference() {
    cube( [127.8, 85.4, 10.4]);
    for( ix = xrange) {
      for( iy = yrange) {
	translate( [xoffset+ix*4.5, yoffset+iy*4.5, -0.1])
	cylinder( h=12, d=3.1);
      }
    }
  }
}

pcb_x = 40;
pcb_y = 85;
pcb_z = 1.6;

pcb_xmarg = 0;
pcb_ymarg = 7.5;

led_x = 5.0;
led_y = 5.2;
led_lip1 = 0.05;

module led() {
  color("#800000") {
    translate( [-led_x/2, -led_y/2, 0]) {
      cube( [led_x, led_y, 0.3]);
      translate( [led_lip1, led_lip1, 0.3])
	cube( [led_x-2*led_lip1, led_y-2*led_lip1, 0.7]);
    }
    translate( [0, 0, 1]) {
      union() {
	cylinder( h=3, d=4);
	translate( [0, 0, 3])
	  sphere( d=4);
      }
    }
  }
}

pcb_hole_offset = 5;
pcb_hole_dia = 2.5;

pcb_wid = pcb_x+2*pcb_xmarg;
pcb_hgt = pcb_y+2*pcb_ymarg;

module pcb1() {
  // color("#208020")
  translate( [-pcb_xmarg, -pcb_ymarg, 0]) {
    difference() {
      %cube([ pcb_wid, pcb_hgt, pcb_z]);
      // mounting holes
      translate( [pcb_hole_offset, pcb_hole_offset, -0.1])
	%cylinder( h=pcb_z+0.2, d=pcb_hole_dia);
      translate( [pcb_wid-pcb_hole_offset, pcb_hole_offset, -0.1])
	%cylinder( h=pcb_z+0.2, d=pcb_hole_dia);
      translate( [pcb_wid-pcb_hole_offset, pcb_hgt-pcb_hole_offset, -0.1])
	%cylinder( h=pcb_z+0.2, d=pcb_hole_dia);
      translate( [pcb_hole_offset, pcb_hgt-pcb_hole_offset, -0.1])
	%cylinder( h=pcb_z+0.2, d=pcb_hole_dia);
    }
  }
  for( ix = [0:2]) {
    for( iy = [0:5]) {
      translate( [xoffset+ix*9, yoffset+iy*9, -0.1])
	rotate( [180, 0, 0])
	led();
    }
  }
  translate( [pcb_wid/2, 0, pcb_z])
    heatsink( 65);
}

translate([ 0, 0, 20])
    pcb1();

plate_mod();


module heatsink(len) {
  color("#9090c0") {
    translate( [-16, 0, 0]) {
      cube([32, len, 8]);
      translate( [0, 0, 8]) {
	for( ix=[0:4]) {
	  translate( [(30/4)*ix, 0, 0])
	    cube( [2, len, 42-8]);
	}
      }
    }
  }
}
